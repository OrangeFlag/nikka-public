#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]

#![no_main]
#![no_std]


use ku::{log::info, time};

use lib::{entry, syscall};


entry!(main);


fn main() {
    loop {
        syscall::sched_yield();
    }
}
